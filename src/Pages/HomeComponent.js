import React from "react";
import "../index.scss";
import  {InputComponent,ToggleSwitchComponent,AlertComponent} from "blueberry-ui";

class HomeComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email:'',
            input: '',
            showNotice:false,
            showModal: false
        };
        this.changeInputValue = this.changeInputValue.bind(this);
        this.changeEmailValue = this.changeEmailValue.bind(this);
        this.closeAction = this.closeAction.bind(this);
        this.submitForm = this.submitForm.bind(this);
    }

    submitForm(e) {
        e.preventDefault();
        const isEmailValid = this.validateEmail(this.state.email);
        const payload = isEmailValid ? {showModal: true}:{showNotice: true};
        this.setState(payload);
    }
    validateEmail(email){
        const emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return emailPattern.test(String(email).toLowerCase());
    }

    changeInputValue(e) {
        this.setState({input: e.target.value});
        console.log(e.target.value)
    }
    changeEmailValue(e) {
        this.setState({email: e.target.value});
    }

    handleToggleClick(e) {
        console.log(e)
    }

    closeAction(e){
        this.setState({showNotice: false,showModal:false})
    }

    render() {
        return (
            <main>
                <h1>Berry UI</h1>
                <p>Berry is a small UI framework for React. Check out the basic components here:</p>

                <form method="post" action="" onSubmit={this.submitForm}>
                    <fieldset>
                        <InputComponent
                            type="text"
                            value={this.state.input}
                            placeholder="Enter your name"
                            onChangeAction={this.changeInputValue}
                            className="text-field"
                        /></fieldset>
                    <fieldset>
                        <InputComponent
                            type="email"
                            value={this.state.email}
                            placeholder="Enter your email"
                            onChangeAction={this.changeEmailValue}
                            className={this.state.showNotice ? 'err':''}
                        /></fieldset>
                    <fieldset>
                        <ToggleSwitchComponent
                            label="ready"
                            onClick={this.handleToggleClick}
                        />
                    </fieldset>
                    <fieldset>
                        <InputComponent type="submit" value="submit"/>
                    </fieldset>
                </form>
                {this.state.showModal ?
                    <AlertComponent
                        type="modal"
                        closeAction={this.closeAction}
                    >
                        <p>Your details were saved, thanks.</p>
                    </AlertComponent> : ''}
                {this.state.showNotice ?
                    <AlertComponent
                        type="notice"
                        className="err"
                        closeAction={this.closeAction}
                    >
                        <p>Please check out your input</p>
                    </AlertComponent> : ''}
            </main>
        )
    }
}


export default HomeComponent;
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
module.exports = {
    mode: "production",
    entry: {
        client:['./src/index.js']
    },
    output: {
        filename: "[name].[contenthash].min.js",
        path: path.join(__dirname, "/public/"),
        publicPath: '/'
    },
    resolve: {
        extensions: ['.js'],
        alias: {
            components: path.resolve(__dirname, '..', 'src/components'),
        }
    },
    plugins:[
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
        title: 'BerryUI',
        meta:{
            viewport:'width=device-width,initial-scale=1.0'
        },
        filename: 'index.html',
        template:'./src/index.html'
    })],
    module: {
        rules: [
            {
                test: /\.js?$/,
                use: [{
                    loader:'babel-loader',
                    options: {
                        presets: ["@babel/preset-env",'@babel/preset-react'],
                        plugins: [
                            '@babel/plugin-proposal-object-rest-spread',
                            "@babel/plugin-transform-runtime"
                        ]
                    }

                }],
                exclude: /node_modules/,
                //include:/node_modules\/berry-ui/,

            },
            {
                test: /\.scss$/,
                use: [
                    'style-loader',
                    'css-loader', {
                    loader: "postcss-loader",
                    options: {
                        plugins: () => [require("autoprefixer")({ grid: true }),
                            require('cssnano')({preset: 'default'})
                        ],
                        minimize: true
                    },
                }, {
                        loader: 'sass-loader',
                        options: {
                            /*data: `
                            @import "src/css/_variables";`*/
                        }
                    }]
            },
            {
                test: /\.(ttf|eot|otf|svg|png|jpg)$/,
                loader: 'file-loader'
            },
            {
                test: /\.(woff|woff2)$/,
                loader: 'url-loader'
            }
        ],

    }
};